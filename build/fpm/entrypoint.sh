#! /bin/bash

set -e

_term() {
    echo "Stopping Process!"
    kill -s SIGQUIT "$fpm" 2>/dev/null
}

trap _term SIGQUIT

if [ ! -f .env ]
then
  pwd
  ls -alh
  cp .env.example .env
  php artisan key:generate || rm .env
fi
php artisan inspire

$@ &

fpm=$!
wait "$fpm"