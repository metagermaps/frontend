<figure id="offline-addon" class="addon">
    <div id="heading">
        <div class="exit">
            <span class="glyphicon glyphicon-arrow-left"></span>
        </div>
        <div class="logo">
            <div>
                <h1>Maps.MetaGer.de<small>Offline</small></h1>
            </div>
        </div>
    </div>
    <div class="results">
        <div class="downloaded-areas">
            <div class="placeholder no-areas">
                <span>Noch kein Gebiet heruntergeladen</span>
            </div>
            <div class="placeholder loading-areas"><img src="/img/ajax-loader.gif" alt="loading" />
                <span>Lade Gebiete</span>
            </div>
            <div class="add-area placeholder">
                <a href="#">+ Gebiet für Download hinzufügen</a>
            </div>
            <!--
                <div class="auto-updates palceholder inactive">
                <div class="option">
                    <label class="switch">
                        <input type="checkbox" checked>
                        <span class="slider round"></span>
                    </label>
                </div>
                <div class="text">
                    <span>Automatische Updates</span>
                </div>
            </div>
            -->
            <div id="area-template" class="area">
                <div>
                    <div class="size"></div>
                    <div class="last-modified"></div>
                </div>
                <div class="name"></div>
                <div class="inspect">
                    <span class="glyphicon glyphicon-search"></span>
                    <img src="/img/ajax-loader.gif" alt="loading" style="display: none;">
                </div>
                <div class="rename" style="display: none;">
                    <span class="glyphicon glyphicon-pencil"></span>
                    <img src="/img/ajax-loader.gif" alt="loading" style="display: none;">
                </div>
                <div class="remove" style="display: none;">
                    <span class="glyphicon glyphicon-trash"></span>
                    <img src="/img/ajax-loader.gif" alt="loading" style="display: none;">
                </div>
            </div>
        </div>
        <div class="area-selection inactive">
            <div class="download-information">
                <div class="size"></div>
            </div>
            <div class="text">
                <p>Bewege die Karte, sodass das herunterzuladende Gebiet angezeigt wird und klicke rechts auf download.
                </p>
            </div>
            <div id="start-download">
                <span class="glyphicon glyphicon-download-alt"></span>
            </div>
        </div>
        <div class="area-downloading inactive">
            <div id="download-information">
                <div class="size"></div>
            </div>
            <div class="text">
                <p>Lädt Karte ...</p>
            </div>
        </div>
        <div class="download-progress inactive">
            <span class="progress-label">0%</span>
            <div class="progress">
                <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"
                    style="width: 0%;"></div>
            </div>
            <div class="abort">
                <span class="glyphicon glyphicon-remove-sign"></span>
            </div>
        </div>
    </div>
</figure>