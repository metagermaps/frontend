<!DOCTYPE html>
<html lang="de">

<head>
    <meta charset="utf-8" />
    <meta content="IE=edge" http-equiv="X-UA-Compatible" />
    <meta content="width=device-width, initial-scale=1, user-scalable=no" name="viewport" />
    <title>
        Maps - MetaGer
    </title>
    @if (isset($css))
    @foreach ($css as $el)
    <link href="{{ $el }}" rel="stylesheet" type="text/css" />
    @endforeach
    @endif
    <meta name="tileserverhost" content="{{ config('maps.tileserver.host') }}">

    @if (isset($query))
    <meta name="start-query" content="{{ $query }}">
    @endif

    <link rel="stylesheet" href="{{ mix('/css/map.css') }}" type="text/css">
</head>

<body>
    <main>
        <div class="map" id="map" tabindex="0">
        </div>
    </main>
</body>
<script async src="{{mix('/js/mapFrame.js')}}"></script>

</html>