<!DOCTYPE html>
<html lang="{{ App::currentLocale() }}" data-fallback_lang="{{ App::getFallbackLocale() }}" data-default_lang="{{ App\Http\Middleware\Localization::getDefaultLocale() }}">

<head>
    <meta charset="utf-8" />
    <meta content="IE=edge" http-equiv="X-UA-Compatible" />
    <meta content="width=device-width, initial-scale=1, user-scalable=no" name="viewport" />
    <meta name="color-scheme" content="light dark">
    <title>
        Maps - MetaGer
    </title>
    @if (isset($css))
        @foreach ($css as $el)
            <link href="{{ $el }}" rel="stylesheet" type="text/css" />
        @endforeach
    @endif
    <meta name="tileserverhost" content="{{ config('maps.tileserver.host') }}">
    <meta name="supported-locales"
        content="{{ base64_encode(json_encode(App\Http\Middleware\Localization::SUPPORTED_LOCALES)) }}">
    @if(isset($geoip))
        <meta name="start-geoip" content="{{ json_encode($geoip) }}">
    @endif
    @if (isset($bbox))
        <meta name="start-bbox" content="{{ $bbox }}">
    @endif
    @if (isset($module))
        <meta name="module" content="{{ $module }}">
    @endif
    @if (isset($query))
        <meta name="start-query" content="{{ $query }}">
    @endif
    @if (isset($viewbox))
        <meta name="start-viewbox" content="{{ $viewbox }}">
    @endif
    @if (isset($reverse_lon) && isset($reverse_lat) && isset($reverse_zoom))
        <meta name="start-reverselon" content="{{ $reverse_lon }}">
        <meta name="start-reverselat" content="{{ $reverse_lat }}">
        <meta name="start-reversezoom" content="{{ $reverse_zoom }}">
    @endif
    @if (isset($waypoints) && isset($vehicle))
        <meta name="start-vehicle" content="{{ $vehicle }}">
        <meta name="start-waypoints" content="{{ $waypoints }}">
    @endif
    @if (Request::filled('bounded') && in_array(Request::input('bounded'), ['enabled', 'disabled']))
        <meta name="start-bounded" content="{{ Request::input('bounded') }}">
    @endif
    @if (isset($navigation_active) && $navigation_active === true)
        <meta name="navigation_active" content="true">
    @endif
    <meta name="env" content="{{ \App::environment() }}">
    <link rel="stylesheet" href="{{ mix('/css/map.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ mix('/css/result.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ mix('/css/nav-arrows.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ mix('/css/navbar.css') }}" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="{{ mix('/css/addons.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ mix('/css/searchaddon.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ mix('/css/routefinder.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ mix('/css/navigation.css') }}" rel="stylesheet" type="text/css">
    @if (!\App::environment('production'))
        <link rel="stylesheet" href="{{ mix('/css/fakegps.css') }}" type="text/css">
    @endif
</head>

<body>
    <div id="page">
        <div class="close-container"><button class="close" aria-label="@lang('close sub page')"
                title="@lang('close sub page')">❌</button></div>
        <iframe src="" frameborder="0"></iframe>
    </div>
    <main>

        @include('addons/search')
        @include('addons/navigation')
        @include('addons/route')
        @include('addons/offline')
        @if (!\App::environment('production'))
            @include('addons/fakegps')
        @endif

        <div class="map" id="map" tabindex="0">
        </div>
        <div id="gps-error" class="hidden">
            <div><span>@lang('MetaGer could not find your exact location')</span> <a href="#">@lang('why?')</a></div>
        </div>
    </main>
    <dialog id="settings">
        <h1>@lang("settings")</h1>
        <p>@lang("usage of cookies in settings")</p>
        <div class="settings-container">
            <div class="setting">
                <label for="language">@lang("language")</label>
                <select name="language" id="language">
                    @foreach(App\Http\Middleware\Localization::SUPPORTED_LOCALES as $supported_locale)
                        <option value="{{$supported_locale}}" @if(App::currentLocale() === $supported_locale)selected @endif>@lang($supported_locale)</option>
                    @endforeach
                </select>
            </div>
            <div class="setting">
                <label for="theme">@lang("theme")</label>
                <select name="theme" id="theme">
                    <option value="light">@lang("light mode")</option>
                    <option value="dark">@lang("dark mode")</option>
                </select>
            </div>
        </div>
        <button class="close">@lang("close")</button>
    </dialog>
    <dialog id="gps-modal">
        <p>@lang('gps description')</p>
        <p>@lang('gps android')</p>
        <button class="close">@lang("close")</button>
    </dialog>
    <dialog id="app-deprecation">
        <h1>Es wird Zeit für ein Update</h1>
        <p>Diese Android App hat bereits seit einigen Jahren keine Updates mehr erhalten. Wir bieten eine neue App an, die mehr Geräte und vor allem alle aktuellen Features unterstützt. </p>
        <p>Aus diesem Grund wird diese App ab dem 01.08.2024 nicht mehr funktionieren. Wir möchten Sie bitten sich bis dahin stattdessen unsere aktuelle App zu installieren, da ein Update unsererseits nicht möglich ist.</p>
        <p>Unsere aktuelle App finden Sie im Google Playstore, oder unter https://metager.de/app</p>
        @if(\Carbon\Carbon::createFromDate(2024, 8, 1, "UTC")->isFuture())
        <div class="options">
            <a href="#" class="btn btn-danger">Später</a>
        </div>
        @endif
    </dialog>
    <div id="nav-menu">
        <input type="checkbox" name="nav-opener" id="nav-opener">
        <label for="nav-opener">≡</label>
        <nav>
            <div class="logo"><img src="/img/metager.svg" alt="MetaGer Logo"></div>
            <ul class="nav-items">
               
                <li><a href="https://metager.de/datenschutz" target="_blank">@lang('privacy')</a></li>
                <li><a href="https://metager.de/impressum" target="_blank">@lang('impress')</a></li>
                @if(App::getLocale() === "de")
                <li><a href="https://metager.de/app" target="_blank">App</a></li>
                @else
                <li><a href="https://metager.org/app" target="_blank">App</a></li>
                @endif
                @if (!\App::environment('production'))
                    <li><a href="#" id="nav-fakegps">Fake GPS</a></li>
                @endif
                <li id="settings-switch"><a href="#">@lang('settings')</a></li>
            </ul>
            <div class="updates">
                <label>@lang('last openstreetmap update')</label>
                <div>@lang('timezone'): <span class="data-date-timezone"></span></div>
                <div>@lang('map'): <span class="data-date-tiles"><img src="/img/ajax-loader.gif" 
a                            lt="@lang('loading data...')"></span>
                </div>
                <div>@lang('search'): <span class="data-date-search"><img src="/img/ajax-loader.gif"
                            alt="@lang('loading data...')"></span>
                </div>
                <div>@lang('routes'): <span class="data-date-routing"><img src="/img/ajax-loader.gif"
                            alt="@lang('loading data...')"></span></div>
            </div>
        </nav>
    </div>
    <script src="/js/turf.min.js" type="text/javascript" defer></script>
    <script src="{{ mix('/js/modules.js') }}" type="text/javascript" defer></script>
    <script src="{{ mix('/js/lib.js') }}" type="text/javascript" defer></script>
    <script src="{{ mix('js/map.js') }}" type="text/javascript" defer></script>
    <script>
        var _paq = window._paq = window._paq || [];
        /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
        _paq.push(["disableCookies"]);
        _paq.push(['trackPageView']);
        _paq.push(['enableLinkTracking']);
        _paq.push(['enableHeartBeatTimer']);
        (function() {
            var u="//stats.suma-ev.de/";
            _paq.push(['setTrackerUrl', u+'matomo.php']);
            _paq.push(['setSiteId', '2']);
            var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
            g.async=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
        })();
    </script>
</body>

</html>