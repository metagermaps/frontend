<!DOCTYPE html>
<html lang="de">

<head>
    <meta charset="utf-8" />
    <meta content="IE=edge" http-equiv="X-UA-Compatible" />
    <meta content="width=device-width, initial-scale=1, user-scalable=no" name="viewport" />
    <meta name="color-scheme" content="light dark">
    <title>
        Maps - MetaGer
    </title>
    <link rel="stylesheet" href="{{ mix("css/staticPages.css") }}">
    <script src="{{ mix("js/static.js") }}" async></script>
</head>

<body>
    <div class="content">
        <div class="panel-heading big-header">GPS und Positionsdaten</div>
        <div class="panel">
            <div class="panel-heading">Wie kann ich meinen Standort für Maps.MetaGer.de einschalten?</div>
            <div class="panel-body">
                <p>Ganz einfach: Schalten sie hierfür lediglich die GPS-Funktion ihres Mobilgeräts ein.</p>
                <p>Für Android Geräte genügt hier ein Wischen vom oberen Bildschirmrand nach unten. Ein Menü erscheint
                    mit
                    verschiedenen Schnellzugriffen. Eins davon nennt sich "GPS". Ein Klick hierauf aktiviert das GPS.
                    Laden
                    Sie nun unsere Seite neu um Zugriff auf nachfolgend genannte Funktionen zu haben.</p>
            </div>
        </div>
        <div class="panel">
            <div class="panel-heading">Welche Vorteile bringt das Einschalten vom GPS?</div>
            <div class="panel-body">
                <p>Hierdurch können zusätzliche Funktionen zur Verfügung gestellt werden:</p>
                <ul>
                    <li>Automatisches zentrieren der Karte um möglichst relevante Suchergebnisse in näherer Umgebung zu
                        finden,
                    </li>
                    <li>Automatisches berechnen der Route vom/zum aktuellen Standort,</li>
                    <li>Freischalten des <a href="#route-assist">Routen-Assistenten</a>, welcher Sie Schritt für
                        Schritt bis zum Ziel navigieren kann.
                    </li>
                </ul>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">Werbt Ihr nicht damit, dass die geographische Position geschützt bleibt?</div>
            <div class="panel-body">
                <p>
                    Das ist korrekt. In den meisten Fällen verlässt die berechnete Position nicht einmal das Gerät auf
                    dem
                    diese abgefragt wird. Und in den Fällen wo sie es tut (z.B. beim Berechnen einer Route vom aktuellen
                    Standort aus), wird diese zuvor in ein Format gebracht, welches sich von uns nicht unterscheiden
                    lässt
                    von den Koordinaten eines jeden anderen Wegpunkts.
                </p>
                <p>
                    Positionsdaten, die uns erreichen, werden dann direkt dafür verwendet z.B. die kürzeste Route zum
                    Ziel
                    zu berechnen (und nur dafür).
                </p>
                <p>
                    Wir schreiben auch keine Logs über die bisher abgefragten Daten. Diese werden tatsächlich nur dafür
                    verwendet, wofür sie sich am Besten eignen: Das Leben eines jeden Nutzers zu erleichtern, ohne dass
                    er
                    dafür seine Daten verkaufen muss.
                </p>
            </div>
        </div>
        <div class="panel-heading big-header" id="route-assist">Routen-Assistent</div>
        <div class="panel panel-default">
            <div class="panel-heading"></div>
            <div class="panel-body">
                <p>Voraussetzung hierfür ist ein halbwegs aktueller mobiler Browser (z.B. Firefox - zu finden im
                    App-Store).</p>
                <p>Plant man nun eine Route zu verschiedenen Wegpunkten, muss der Startpunkt die aktuelle GPS-Position
                    sein. Bei eingeschaltetem GPS auf dem Gerät erscheint eine entsprechende Option, wenn man die
                    Eingabe für den Wegpunkt startet.</p>
                <p>Alle weiteren Wegpunkte können wie gewohnt beliebig entweder über die Suchfunktion innerhalb der
                    Eingabebox für Wegpunkte im Routenplaner, oder durch einen Klick auf die Karte hinzugefügt werden.
                </p>
                <p>Nach einem Klick auf "Route berechnen" erscheint nun die Wegbeschreibung und zusätzlich darüber ein
                    Knopf "Navigation starten".</p>
                <p>Ein Klick auf diesen Knopf startet dann die Navigation.</p>
            </div>
        </div>
    </div>
</body>

</html>