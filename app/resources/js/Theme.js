import { map } from "./maps/MetaGerMapModule";

/**
 * Checks if the user has defined a custom theme option light|dark
 * and returns the current setting.
 * 
 * Also applies the theme value
 * 
 * @returns {string}
 */
export function detectTheme() {
    // Check if the user has overwritten the color-scheme
    let theme = "light";
    if (localStorage)
        theme = localStorage.getItem("color-scheme");

    if (theme == null || !["light", "dark"].includes(theme)) {
        theme = getDefaultTheme();
    }
    document.querySelector("meta[name=color-scheme]").content = theme;
    document.querySelector("body").dataset.theme = theme;

    return theme;
}

function getDefaultTheme() {
    if (window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches) {
        return "dark";
    } else {
        return "light";
    }
}

export function toggleTheme() {
    let theme = detectTheme();
    if (theme == "light") {
        theme = "dark";
    } else {
        theme = "light";
    }
    if (getDefaultTheme() == theme) {
        localStorage.removeItem("color-scheme");
    } else {
        localStorage.setItem("color-scheme", theme);
    }
    map.switchTheme(theme).then(() => {
        detectTheme();
        if (map.hoverManager) {
            map.hoverManager.enable();
        }
    });
}