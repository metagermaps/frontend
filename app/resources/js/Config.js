import { bbox, bboxPolygon, circle } from "@turf/turf";
import { LngLatBounds, CenterZoomBearing, LngLat } from "maplibre-gl";
import { gpsManager } from "./GpsManager";
import { UrlGenerator } from "./UrlGenerator";
import { detectTheme, toggleTheme } from "./Theme";

export const config = {
  localization: {
    current_locale: document.querySelector("html").getAttribute("lang"),
    fallback_locale: document.querySelector("html").dataset.fallback_lang,
    default_locale: document.querySelector("html").dataset.default_lang,
    supported_locales: JSON.parse(atob(document.querySelector("meta[name=supported-locales]").content))
  },
  env: document.querySelector("meta[name=env]").content,
  double_click_timeout_ms: 250,
  init: {
    /** @type {LngLatBounds} */
    bbox: null,
    /** @type {LngLatBounds} */
    viewbox: null,
    /** @type {LngLatBounds} */
    guessed_user_location: null,
    bounded: null,
    query: null,
    /** @type {CenterZoomBearing} */
    reverse: null,
    vehicle: null,
    waypoints: null,
    navigation_active: false,
    module: null,
  },
};

let start_bbox = document.querySelector("meta[name=start-bbox]");
if (start_bbox) {
  try {
    start_bbox = JSON.parse(start_bbox.content);
    config.init.bbox = new LngLatBounds(start_bbox[0].concat(start_bbox[1]));
  } catch (error) {
    // Probably no valid JSON
    console.error(`${start_bbox.content} is not a valid JSON boundingbox`);
  }
}

let viewbox = document.querySelector("meta[name=start-viewbox]");
if (viewbox) {
  // Parse viewbox
  try {
    viewbox = JSON.parse(viewbox.content);
    viewbox = LngLatBounds.convert(viewbox);
    config.init.viewbox = viewbox;
  } catch (e) { }
}

// Store the user location we have guessed to fit
// If we have permission to use geolocation: try to fetch a quick position
await new Promise((resolve, reject) => {
  gpsManager.hasPermissions().then(has_permissions => {
    if (has_permissions == null || !has_permissions) return reject("No Geolocation Permission");
    gpsManager.getCurrentPosition(location => {
      let min_radius = location.coords.accuracy < 500 ? 500 : 20000;
      let radius = Math.max(min_radius, location.coords.accuracy) / 1000.0;
      let bbox_polygon = bbox(circle([location.coords.longitude, location.coords.latitude], radius));
      config.init.guessed_user_location = new LngLatBounds(bbox_polygon);
      resolve();
    }, error => {
      return reject(error);
    }, {
      enableHighAccuracy: false,  // High accuracy location results usually take too long. We want a quick and rough position
      timeout: 500,         // As this is a blocking request we won't wait for too long for a location result
      maximumAge: 3600000,  // Accept old position up to one hour
    });
  });
}).catch(error => {
  // No geolocation permission or timeout probably
  // Maybe we have a maxmind geoip parsed
  let start_geoip = document.querySelector("meta[name=start-geoip]");
  if (start_geoip) {
    try {
      let geoip = JSON.parse(start_geoip.content);
      let radius = Math.max(20000, geoip.accuracy) / 1000.0;
      let bbox_polygon = bbox(circle([geoip.longitude, geoip.latitude], radius));
      config.init.guessed_user_location = new LngLatBounds(bbox_polygon);
    } catch (error) {
      console.error(error);
    }

  } else {
    console.error(error);
  }
});

let bounded = document.querySelector("meta[name=start-bounded]");
if (bounded) {
  config.init.bounded = bounded.content;
}

let query = document.querySelector("meta[name=start-query]");
if (query) {
  config.init.query = query.content;
}

// Parse reverse
let reverse_lon = document.querySelector("meta[name=start-reverselon]");
let reverse_lat = document.querySelector("meta[name=start-reverselat]");
let reverse_zoom = document.querySelector("meta[name=start-reversezoom]");

if (reverse_lon && reverse_lat && reverse_zoom) {
  config.init.reverse = {
    bearing: 0,
    center: new LngLat(parseFloat(reverse_lon.content), parseFloat(reverse_lat.content)),
    zoom: parseFloat(reverse_zoom.content),
  };
}
if (reverse_lon) {
  config.init.reverse_lon = parseFloat(reverse_lon.content);
}
if (reverse_lat) {
  config.init.reverse_lat = parseFloat(reverse_lat.content);
}
if (reverse_zoom) {
  config.init.reverse_zoom = reverse_zoom.content;
}
let vehicle = document.querySelector("meta[name=start-vehicle]");
if (vehicle) {
  config.init.vehicle = vehicle.content;
}
let waypoints = document.querySelector("meta[name=start-waypoints]");
if (waypoints) {
  config.init.waypoints = waypoints.content;
}

let navigation_active = document.querySelector("meta[name=navigation_active]");
if (navigation_active) {
  config.init.navigation_active = true;
}

let module = document.querySelector("meta[name=module]");
if (module) {
  config.init.module = module.content;
}

/**
 * Settings module functionality
 */
(() => {
  let settings_container = document.getElementById("settings");
  if ((new URLSearchParams(location.search)).has("settings")) {
    settings_container.showModal();
  }
  // Make settings open button work
  document.querySelector("#settings-switch > a").addEventListener("click", e => {
    e.preventDefault();
    document.querySelector("#nav-opener").checked = false;
    settings_container.showModal();

    let current_url = new URL(location);
    let search_params = new URLSearchParams(current_url.search);
    search_params.set("settings", "");
    current_url.search = `?${search_params.toString()}`;
    let state = history.state;
    if (state) {
      if (state.hasOwnProperty("options")) {
        state.options.settings = true;
      } else {
        state.options = { settings: true };
      }
    }
    history.pushState(state, "", current_url);
  });
  // Make settings close button work
  settings_container.querySelector("button.close").addEventListener("click", e => {
    settings_container.close();
    let current_url = new URL(location);
    let search_params = new URLSearchParams(current_url.search);
    search_params.delete("settings");
    if (search_params.size > 0) {
      current_url.search = `?${search_params.toString()}`;
    } else {
      current_url.search = "";
    }
    let state = history.state;
    if (state) {
      if (state.hasOwnProperty("options")) {
        state.options.settings = false;
      } else {
        state.options = { settings: false };
      }
    }
    history.pushState(state, "", current_url);
  });

  // Make Language switcher work
  let language_switch_select = settings_container.querySelector(".settings-container #language");
  if (language_switch_select) {
    language_switch_select.value = config.localization.current_locale;
    language_switch_select.addEventListener("change", e => {
      let new_language = e.target.value;
      if (new_language != config.localization.current_locale) {
        if (config.localization.default_locale !== new_language) {
          setCookie("locale", new_language);
        } else {
          removeCookie("locale");
        }
        let new_url = UrlGenerator.to(location.pathname + location.search, new_language);
        location.replace(new_url);
      }
    });
  } else { console.error("Language switch is not present") }

  // Make theme option work
  let theme_switch_select = settings_container.querySelector(".settings-container #theme");
  if (theme_switch_select) {
    theme_switch_select.value = detectTheme();
    theme_switch_select.addEventListener("change", e => {
      toggleTheme();
    });
  } else { console.error("Theme switch is not present") }

  function setCookie(name, value, expiration_ms = 5 * 365 * 24 * 60 * 60 * 1000) {
    let expiry = new Date();
    expiry.setTime(expiry.getTime() + expiration_ms);
    document.cookie = `${name}=${value}; expires=${expiry.toUTCString()}; path=/`;
  }
  function removeCookie(name) {
    setCookie(name, "deleted", 0);
  }
})();

(async () => {
  // Parse dynamic insets
  let search_params = new URLSearchParams((new URL(location)).search);
  if (search_params.get("insets") != null) {
    let insets = search_params.get("insets").split(",");
    if (insets.length != 4) return;
    for (let inset of insets) {
      if (isNaN(inset)) return;
    }
    document.querySelector("body").style.setProperty("--inset-top", insets[0] + "px");
    document.querySelector("body").style.setProperty("--inset-right", insets[1] + "px");
    document.querySelector("body").style.setProperty("--inset-bottom", insets[2] + "px");
    document.querySelector("body").style.setProperty("--inset-left", insets[3] + "px");
  }
})();