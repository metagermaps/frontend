export class Wikidata {
    label = null;
    description = null;

    /**
     * 
     * @param {string} image_url 
     * @param {object} attribution 
     * 
     * @returns {HTMLDivElement}
     */
    static GENERATE_IMAGE_CONTAINER(image_url, attribution) {
        let image_container = document.createElement("div");
        image_container.classList.add("wikidata-image");

        let image_link = document.createElement("a");
        image_link.href = attribution.wikidata.link;
        image_link.target = "_blank";
        image_link.rel = "noopener";
        image_link.title = attribution.wikidata.name;

        let flag_image = document.createElement("img");
        flag_image.src = image_url;
        flag_image.alt = attribution.wikidata.name;
        image_link.appendChild(flag_image)

        image_container.appendChild(image_link);

        // Create Attribution
        let attribution_container = document.createElement("div");
        attribution_container.classList.add("wikidata-image-attribution");

        if (attribution.artist) {
            let artistContainer = document.createElement("div");
            artistContainer.classList.add("wikidata-image-attribution-artist");
            artistContainer.innerHTML = attribution.artist;
            let anchor = artistContainer.querySelector("a");
            if (anchor) {
                anchor.target = "_blank";
                anchor.rel = "noopener";
            }
            attribution_container.appendChild(artistContainer);
        }

        let wikidata_link_container = document.createElement("a");
        wikidata_link_container.href = attribution.wikidata.link;
        wikidata_link_container.target = "_blank";
        wikidata_link_container.rel = "noopener";
        wikidata_link_container.textContent = attribution.wikidata.name;
        attribution_container.appendChild(wikidata_link_container);

        if (attribution.license) {
            let license_container = document.createElement("a");
            license_container.classList.add("wikidata-iamge-attribution-license");
            license_container.target = "_blank";
            license_container.rel = "noopener";
            license_container.href = attribution.license.link;
            license_container.textContent = attribution.license.name;
            attribution_container.appendChild(license_container);
        }
        image_container.appendChild(attribution_container);

        return image_container;
    }
}