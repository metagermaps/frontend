import { config } from "./Config";
import { Result } from "./Result";
import { SearchModule } from "./SearchModule";
import { UrlGenerator } from "./UrlGenerator";
import { module } from "./app";
import { map } from "./maps/MetaGerMapModule";
import { LngLat, LngLatBounds } from "maplibre-gl";

export class Search {
  /**
   * Entrypoint for a search for any user entered query
   *
   * @param {string} query
   * @param {LngLatBounds} viewbox
   * @returns {Promise<Result[]>}
   */
  static async SEARCH(query, viewbox) {
    if (query.match(/^((([NWR]\d+)|inview|filter),?)*$/)) {
      // Search for specific OSM ID
      let osm_ids = [];
      let filter = false; // Filters none found osm_ids and updates the query string accordingly
      let inview = false; // Only returns results that are within the current map view
      for (let osm_id of query.split(',')) {
        if (osm_id == "filter") filter = true;
        else if (osm_id == "inview") inview = true;
        else osm_ids.push(osm_id);
      }
      return Search.LOOKUP(osm_ids).then(results => {
        let new_results = [];
        let result_ids = [];
        let bounds = map.getBounds();
        for (let result of results) {
          if (inview && !bounds.contains(result.location)) continue;
          result_ids.push(result.toString());
          new_results.push(result);
        }
        if (filter && module instanceof SearchModule) {
          module.query = result_ids.join(",");
          module.searchinput.value = module.query;
        }
        return new_results;
      });
    } else {
      // Fulltext search
      return Search.FULLTEXT(query, viewbox);
    }
  }

  /**
   * Entrypoint for a search for results which queries Photon and Nominatim
   *
   * @param {string} query
   * @param {LngLatBounds} viewbox
   * @returns {Promise<Result[]>}
   */
  static async FULLTEXT(query, viewbox) {
    let photon_promise = Search.PHOTON(query, viewbox);
    let nominatim_promise = Search.NOMINATIM(query, viewbox);
    return nominatim_promise.then((nominatim_results) => {
      return photon_promise.then((photon_results) => {
        let results = nominatim_results.concat(photon_results);
        // Remove all duplicates
        let existing_ids = [];
        results = results.filter((result) => {
          let already_exists = existing_ids.includes(result.toString());

          if (already_exists) {
            return false;
          } else {
            existing_ids.push(result.toString());
            return true;
          }
        });
        return results;
      });
    });
  }

  /**
   * Entrypoint for a search for results which queries Photon
   *
   * @param {string} query
   * @param {LngLatBounds} viewbox
   * @returns {Promise<Result[]>}
   */
  static async PHOTON(query, viewbox) {
    let camera = map.cameraForBounds(viewbox);

    let params = {
      q: query,
      lon: camera.center.lng,
      lat: camera.center.lat,
      zoom: Math.round(camera.zoom),
      location_bias_scale: 0.4,
      limit: 10,
      locale: config.localization.current_locale
    };

    // Photon search which is very strong for street/city etc search

    return fetch(UrlGenerator.to("/suggest/api"), {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(params),
    })
      .then((response) => response.json())
      .then((response) => {
        let result_ids = [];
        for (const result_json of response.features) {
          result_ids.push(
            result_json.properties.osm_type[0].toUpperCase() +
            result_json.properties.osm_id
          );
        }
        return Search.LOOKUP(result_ids);
      });
  }

  /**
   * Entrypoint for a search for results which queries Photon
   *
   * @param {string} query
   * @param {LngLatBounds} viewbox
   * @returns {Promise<Result[]>}
   */
  static async NOMINATIM(query, viewbox) {
    let camera = map.cameraForBounds(viewbox);
    return fetch(
      UrlGenerator.to("/search?" +
        new URLSearchParams({
          q: `${query} ${camera.center.lat},${camera.center.lng}`,
          dedupe: 1,
          addressdetails: 1,
          extratags: 1,
          polygon_geojson: 1,
          format: "json",
          locale: config.localization.current_locale
        }).toString())
    )
      .then((response) => response.json())
      .then((response) => {
        let results = [];
        for (const result of response) {
          let new_result = new Result(result, true, Result.SOURCE_NOMINATIM);
          results.push(new_result);
        }
        return results;
      })
      .then(async (results) => {
        let load_promises = [];
        for (const result of results) {
          load_promises.push(result.load());
        }
        return Promise.all(load_promises).then(() => {
          return results;
        });
      })
      .catch((error) => {
        // Return empty resultset if something goes wrong
        console.error(error);
        return [];
      });
  }

  /**
   * Entrypoint for a search for results which queries Photon
   *
   * @param {string[]} osm_ids
   * @returns {Promise<Result[]>}
   */
  static async LOOKUP(osm_ids, actions_enabled = true) {
    let params = new URLSearchParams({
      osm_ids: osm_ids.join(","),
      format: "json",
      extratags: 1,
      addressdetails: 1,
      namedetails: 1,
      polygon_geojson: 1,
      locale: config.localization.current_locale
    });

    return fetch(UrlGenerator.to("/lookup" + "?" + params.toString()))
      .then((response) => response.json())
      .then((response) => {
        if (!response) {
          throw new Error(
            `Cannot find Result for OSM Objects: "${osm_ids.join(",")}"`
          );
        }

        let results = [];
        for (const result of response) {
          let new_result = new Result(
            result,
            actions_enabled,
            Result.SOURCE_NOMINATIM
          );
          results.push(new_result);
        }

        return results;
      })
      .then(async (results) => {
        let load_promises = [];
        for (const result of results) {
          load_promises.push(result.load());
        }
        return Promise.all(load_promises).then(() => {
          return results;
        });
      })
      .catch((error) => {
        // Return empty resultset if something goes wrong
        console.error(error);
        return [];
      });
  }

  /**
   * Entrypoint for a search for results which queries Photon
   *
   * @param {LngLat} location
   * @param {number} zoom
   * @returns {Promise<Result[]>}
   */
  static async REVERSE(location, zoom) {
    let params = new URLSearchParams({
      lon: location.lng,
      lat: location.lat,
      zoom: zoom,
      format: "jsonv2",
      dedupe: 1,
      extratags: 1,
      addressdetails: 1,
      namedetails: 1,
      polygon_geojson: 1,
      locale: config.localization.current_locale
    });

    return fetch(UrlGenerator.to("/reverse/" + "?" + params.toString()))
      .then((response) => response.json())
      .then((response) => {
        let result = new Result(response, true, Result.SOURCE_NOMINATIM);
        return result.load().then(() => {
          return result;
        });
      })
      .catch((error) => {
        // Return empty resultset if something goes wrong
        console.error(error);
        return null;
      });
  }
}
