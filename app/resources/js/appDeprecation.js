export function showAppDeprecation() {
    let deprecation_container = document.getElementById("app-deprecation");
    if (!deprecation_container || typeof android == "undefined") return;

    if (localStorage) {
        let ask_time = localStorage.getItem("deprecation_ask_time");
        if (ask_time == null || (new Date()).getTime() > ask_time) {
            deprecation_container.showModal();
        }
    }

    let later_button = deprecation_container.querySelector("a.btn-danger");
    later_button.addEventListener("click", e => {
        e.preventDefault();
        deprecation_container.close();
    });

    deprecation_container.addEventListener("close", e => {
        // Store the time so we do not ask too often
        let next_deprecation_ask = (new Date()).getTime();
        next_deprecation_ask += 24 * 60 * 60 * 1000;    // Ask again in 24 hours

        if (localStorage) {
            localStorage.setItem("deprecation_ask_time", next_deprecation_ask);
        }
    });
}