import { config } from "./Config";
import { detectTheme } from "./Theme";

export class HoverManager {

    #hoverCallBack = this.#onmousemove.bind(this);
    #leaveCallBack = this.#onmouseleave.bind(this);
    #clickCallBack = this.#onclick.bind(this);
    #clickTimeout = null;

    #source_name = "openmaptiles";
    static layers = ["poi-level-3", "poi-level-2", "poi-level-1"]

    _map = null;
    _hover_id = null;
    _hover_sourceLayer = null;

    constructor(map) {
        this._map = map;
        this.enable();
    }

    enable() {
        for (let layer of HoverManager.layers) {
            // Add hover listener
            // Handle feature state changes on hover
            this._map.off("mousemove", layer, this.#hoverCallBack);
            this._map.on("mousemove", layer, this.#hoverCallBack);
            this._map.off("mouseleave", layer, this.#leaveCallBack);
            this._map.on("mouseleave", layer, this.#leaveCallBack);
            this._map.off("click", layer, this.#clickCallBack);
            this._map.on("click", layer, this.#clickCallBack);
            let map_layer = this._map.getLayer(layer);
            if (!map_layer) continue;
            let paint_color = this._map.getPaintProperty(layer, "text-color");
            // Define text color used when feature state is on hover
            this._map.setPaintProperty(layer, "text-color", ["case", ["boolean", ["feature-state", "hover"], false], detectTheme() == "light" ? "rgba(210, 45, 45,1)" : "rgba(206, 126, 126,1)", paint_color]);
        }
    }
    disable() {

    }

    #onmousemove(e) {
        if (!e.features || e.features.length == 0) return;
        let feature = e.features[0];
        if (!feature.id) return;
        this.#setHover(feature.id, feature.sourceLayer);

    }
    #onmouseleave(e) {
        this.#clearHover();
    }
    #onclick(e) {
        // Make sure not to react on double click
        if (this.#clickTimeout != null) {
            clearTimeout(this.#clickTimeout);
            this.#clickTimeout = null;
            return;
        }
        this.#clickTimeout = setTimeout(() => {
            this.#clickTimeout = null;
            let features = this._map.queryRenderedFeatures(e.point, { layers: HoverManager.layers });
            if (features.length == 0) return;
            // Fire the click event so the corresponding module can handle it their way
            let event = new CustomEvent("poiclick", { detail: features[0] });
            this._map.getContainer().dispatchEvent(event);
        }, config.double_click_timeout_ms);

    }

    #setHover(feature_id, source_layer) {
        this.#clearHover();
        this._hover_id = feature_id;
        this._hover_sourceLayer = source_layer;
        this._map.setFeatureState({ source: this.#source_name, sourceLayer: this._hover_sourceLayer, id: this._hover_id }, { hover: true });
        this._map.getCanvas().style.cursor = 'pointer'
    }

    #clearHover() {
        if (this._hover_id) {
            this._map.setFeatureState({ source: this.#source_name, sourceLayer: this._hover_sourceLayer, id: this._hover_id }, { hover: false });
            this._hover_id = null;
            this._hover_sourceLayer = null;
            this._map.getCanvas().style.cursor = '';
        }
    }
}