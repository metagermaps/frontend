function OfflineModuleAndroidConnector() { }

OfflineModuleAndroidConnector.prototype.getExtractList = function () {
  if (
    typeof android != "undefined" &&
    typeof android["getExtractList"] == "function"
  ) {
    return android.getExtractList();
  } else {
    throw 'Android does not have function "getExtractList"';
  }
};

OfflineModuleAndroidConnector.prototype.renameExtract = function (
  oldName,
  newName
) {
  return android.renameExtract(oldName, newName);
};

OfflineModuleAndroidConnector.prototype.removeExtract = function (name) {
  if (
    typeof android != "undefined" &&
    typeof android["removeExtract"] == "function"
  ) {
    return android.removeExtract(name);
  } else {
    throw 'Android does not have function "removeExtract"';
  }
};

OfflineModuleAndroidConnector.prototype.isWireless = function () {
  if (
    typeof android != "undefined" &&
    typeof android["isWireless"] == "function"
  ) {
    return android.isWireless();
  } else {
    throw 'Android does not have function "isWireless"';
  }
};

OfflineModuleAndroidConnector.prototype.isInternetAvailable = function () {
  if (
    typeof android != "undefined" &&
    typeof android["isInternetAvailable"] == "function"
  ) {
    return android.isInternetAvailable();
  } else {
    throw 'Android does not have function "isInternetAvailable"';
  }
};

/**
 * @return Expected Download size or -1 on error
 */
OfflineModuleAndroidConnector.prototype.getExpectedDownloadSize = function (
  boundingBox
) {
  if (
    typeof android != "undefined" &&
    typeof android["getExpectedDownloadSize"] == "function"
  ) {
    return android.getExpectedDownloadSize(
      boundingBox.minLon,
      boundingBox.maxLon,
      boundingBox.minLat,
      boundingBox.maxLat
    );
  } else {
    throw 'Android does not have function "getExpectedDownloadSize"';
  }
};

OfflineModuleAndroidConnector.prototype.downloadFonts = function () {
  if (
    typeof android != "undefined"
  ) {
    return android.downloadFonts();
  } else {
    return false;
  }
}

OfflineModuleAndroidConnector.prototype.downloadTilesInBoundingBox = function (
  boundingBox,
  extractName
) {
  if (
    typeof android != "undefined" &&
    typeof android["downloadTilesInLonLatBounds"] == "function"
  ) {
    return android.downloadTilesInLonLatBounds(
      boundingBox.minLon,
      boundingBox.maxLon,
      boundingBox.minLat,
      boundingBox.maxLat,
      extractName
    );
  } else {
    throw 'Android does not have function "downloadTilesInLonLatBounds"';
  }
};

OfflineModuleAndroidConnector.prototype.cancelDownload = function () {
  if (
    typeof android != "undefined" &&
    typeof android["cancelDownload"] == "function"
  ) {
    return android.cancelDownload();
  } else {
    throw 'Android does not have function "cancelDownload"';
  }
};

OfflineModuleAndroidConnector.prototype.getDownloadPercentage = function () {
  if (
    typeof android != "undefined" &&
    typeof android["getDownloadPercentage"] == "function"
  ) {
    return android.getDownloadPercentage();
  } else {
    throw 'Android does not have function "getDownloadPercentage"';
  }
};

OfflineModuleAndroidConnector.prototype.getDownloadStatusMessage = function () {
  if (
    typeof android != "undefined" &&
    typeof android["getDownloadStatusMessage"] == "function"
  ) {
    return android.getDownloadStatusMessage();
  } else {
    throw 'Android does not have function "getDownloadStatusMessage"';
  }
};

OfflineModuleAndroidConnector.prototype.isDownloadFinished = function () {
  if (
    typeof android != "undefined" &&
    typeof android["isDownloadFinished"] == "function"
  ) {
    return android.isDownloadFinished();
  } else {
    throw 'Android does not have function "isDownloadFinished"';
  }
};

OfflineModuleAndroidConnector.prototype.updateOfflineData = function () {
  if (
    typeof android != "undefined" &&
    typeof android["updateOfflineData"] == "function"
  ) {
    return android.updateOfflineData();
  } else {
    throw 'Android does not have function "updateOfflineData"';
  }
};

OfflineModuleAndroidConnector.prototype.getStage = function () {
  if (
    typeof android != "undefined" &&
    typeof android["getStage"] == "function"
  ) {
    return android.getStage();
  } else {
    throw 'Android does not have function "getStage"';
  }
};

OfflineModuleAndroidConnector.prototype.getMaxStage = function () {
  if (
    typeof android != "undefined" &&
    typeof android["getMaxStage"] == "function"
  ) {
    return android.getMaxStage();
  } else {
    throw 'Android does not have function "getMaxStage"';
  }
};

OfflineModuleAndroidConnector.prototype.getError = function () {
  if (
    typeof android != "undefined" &&
    typeof android["getError"] == "function"
  ) {
    return android.getError();
  } else {
    throw 'Android does not have function "getError"';
  }
};
