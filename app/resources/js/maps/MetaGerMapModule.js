import { MetaGerMap } from "./MetaGerMap";
import { Maplibre } from "./Maplibre";
import { Openlayers } from "./Openlayers";

function isWebglSupported() {
  /**
   * Check for WebGL Support
   */
  let webgl_supported = false;
  var canvas = document.createElement("canvas");
  var gl =
    canvas.getContext("webgl") || canvas.getContext("experimental-webgl");
  if (gl && gl instanceof WebGLRenderingContext) {
    webgl_supported = true;
  }
  canvas = undefined;
  return webgl_supported;
}

/** @type MetaGerMap */
export const map = isWebglSupported() ? new Maplibre() : new Openlayers();
