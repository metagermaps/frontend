import { LngLatBounds, CenterZoomBearing, LngLat } from "maplibre-gl";
import { RenderOptions } from "./RenderOptions";
import { Map } from "ol";
import TileLayer from "ol/layer/Tile";

/**
 * Abstract map class
 *
 * @class Map
 */
export class MetaGerMap {
  static CONTROL_NAVIGATION = "CONTROL_NAVIGATION";
  static CONTROL_SCALE = "CONTROL_SCALE";
  static CONTROL_GEOLOCATE = "CONTROL_GEOLOCATE";
  static CONTROL_ATTRIBUTION = "CONTROL_ATTRIBUTION";

  _tileserver_host = document.querySelector("meta[name=tileserverhost]")
    .content;

  // The map object of the implementation
  _map = null;
  _controls = {
    zoomControl: null,
  };

  constructor() {
    if (this.constructor == Map) {
      throw new Error("Abstract classes cannot be instantiated");
    }
  }

  async load() {
    throw new Error("Abstract function load must be implemented");
  }

  /**
   * Calculates required map padding based on the
   * size of the active module.
   * Optionally adds the padding from parameters to that value.
   * 
   * @param {import("maplibre-gl").PaddingOptions|number} additional_padding 
   * @returns {import("maplibre-gl").PaddingOptions}
   */
  getContentPadding(additional_padding = { top: 0, right: 0, bottom: 0, left: 0 }) {
    if (typeof additional_padding == "number") {
      additional_padding = { top: additional_padding, right: additional_padding, bottom: additional_padding, left: additional_padding };
    }
    if (!additional_padding.hasOwnProperty("top")) additional_padding.top = 0;
    if (!additional_padding.hasOwnProperty("right")) additional_padding.right = 0;
    if (!additional_padding.hasOwnProperty("bottom")) additional_padding.bottom = 0;
    if (!additional_padding.hasOwnProperty("left")) additional_padding.left = 0;

    let body_style = window.getComputedStyle(document.querySelector("body"));

    additional_padding.top += parseInt(body_style.getPropertyValue("--inset-top").replace("px", ""));
    additional_padding.right += parseInt(body_style.getPropertyValue("--inset-right").replace("px", ""));
    additional_padding.bottom += parseInt(body_style.getPropertyValue("--inset-bottom").replace("px", ""));
    additional_padding.left += parseInt(body_style.getPropertyValue("--inset-left").replace("px", ""));

    let active_module = document.querySelector("figure.addon.active");
    if (!active_module) return additional_padding; // In case no module is active: no additional padding needs to be added
    let left = parseInt(window.getComputedStyle(active_module).left.replace("px", ""));
    if (isNaN(left)) {
      left = 0;
    }
    let top = parseInt(window.getComputedStyle(active_module).top.replace("px", ""));
    if (isNaN(top)) {
      top = 0;
    }
    // Depending on the width of the active module we want to focus the map to the right of the module or to the bottom of it
    let client_width = document.querySelector("html").clientWidth;
    let module_width = active_module.clientWidth;
    // If the module is smaller than 40% of the screen we will focus to the right of it. Otherwise to the bottom
    if (module_width / client_width <= 0.41) {
      additional_padding.left = left + module_width;
      additional_padding.top = top;
    } else {
      additional_padding.top = top + active_module.clientHeight;
    }
    return additional_padding;
  }

  /**
   * Sets the padding of the map to account for interface elements
   */
  async updatePadding() {
    throw new Error("Abstract function updatePadding must be implemented");
  }

  /**
   * Returns the map's containing HTML element.
   * 
   * @returns {HTMLElement}
   */
  getContainer() {
    throw new Error("Abstract function getContainer must be implemented");
  }

  /**
   *
   * @param {import("maplibre-gl").LngLatBoundsLike} bounds
   * @param {import("maplibre-gl").FitBoundsOptions} options
   */
  fitBounds(bounds, options) {
    throw new Error("Abstract function fitBounds must be implemented");
  }

  /**
   *
   * @param {import("maplibre-gl").JumpToOptions} options
   * @param {*} eventData
   */
  jumpTo(options, eventData) {
    throw new Error("Abstract function jumpTo must be implemented");
  }

  /**
   *
   * @param {import("maplibre-gl").AnimationOptions|CenterZoomBearing} options
   * @param {*} eventData
   */
  easeTo(options, eventData) {
    throw new Error("Abstract function easeTo must be implemented");
  }

  /**
   * resets pitch and rotation of the map
   */
  resetNorthPitch() {
    throw new Error("Abstract function resetNorthPitch must be implemented");
  }

  /**
   * @returns {LngLatBounds}
   */
  getBounds() {
    throw new Error("Abstract function getBounds must be implemented");
  }

  /**
   * @returns {number}
   */
  getZoom() {
    throw new Error("Abstract function getZoom must be implemented");
  }

  /**
   * @returns {LngLat}
   */
  getCenter() {
    throw new Error("Abstract function getZoom must be implemented");
  }

  /**
   * @returns {number}
   */
  getBearing() {
    throw new Error("Abstract function getBearing must be implemented");
  }

  /**
   * Returns the map viewport height in pixels
   *
   * @returns {number}
   */
  getHeight() {
    throw new Error("Abstract function getHeight must be implemented");
  }

  /**
   * Returns Pixel coordinates for specified geographic coordinates
   *
   * @param {LngLat} coordinates
   */
  project(coordinates) {
    throw new Error("Abstract function project must be implemented");
  }

  /**
   *
   * @param {import("maplibre-gl").LngLatBounds} bounds
   * @returns {CenterZoomBearing}
   */
  cameraForBounds(bounds) {
    throw new Error("Abstract function cameraForBounds must be implemented");
  }

  /**
   * Draws a Featurecollection onto the map under a given id
   * used to be able to remove those features from the map again
   *
   * @param {string} layer_id
   * @param {*} featureCollection
   * @param {RenderOptions} render_options
   * @param {boolean} cluster If features should be clustered together
   */
  drawFeatures(layer_id, featureCollection, render_options, cluster) {
    throw new Error("Abstract function drawFeatures must be implemented");
  }

  removeFeatures(id) {
    throw new Error("Abstract function removeFeatures must be implemented");
  }

  async switchTheme(theme) {
    throw new Error("Abstract function switchTheme must be implemented");
  }

  on(event_name, callback) {
    throw new Error("Abstract function on must be implemented");
  }

  once(event_name, callback) {
    throw new Error("Abstract function once must be implemented");
  }

  /**
   * Removes a previously added event listener
   *
   * @param {*} event_name
   * @param {*} callback
   */
  off(event_name, callback) {
    throw new Error("Abstract function off must be implemented");
  }

  /**
   *
   * @param {boolean} enabled
   */
  toggleDragpan(enabled) {
    throw new Error("Abstract function toggleDragpan must be implemented");
  }

  /**
   *
   * @param {boolean} enabled
   */
  toggleScrollzoom(enabled) {
    throw new Error("Abstract function toggleScrollzoom must be implemented");
  }

  /**
   *
   * @param {boolean} enabled
   */
  toggleDragRotate(enabled) {
    throw new Error("Abstract function toggleDragRotate must be implemented");
  }

  /**
   *
   * @param {boolean} enabled
   */
  toggleKeyboard(enabled) {
    throw new Error("Abstract function toggleKeyboard must be implemented");
  }

  /**
   *
   * @param {LngLat} point
   * @param {number} bearing
   * @param {number} accuracy
   */
  showUserPosition(point, bearing, accuracy) {
    throw new Error("Abstract function showUserPosition must be implemented");
  }

  /**
   *
   * @param {LngLat} point
   * @param {number} bearing
   * @param {number} accuracy
   */
  hideUserPosition() {
    throw new Error("Abstract function showUserPosition must be implemented");
  }

  /**
   *
   * @param {HTMLDivElement} dom_element
   * @param {LngLat} position
   */
  createPopup(dom_element, position) {
    throw new Error("Abstract function createPopup must be implemented");
  }

  /**
   * Removes a popup previously created by createPopup
   * the popup argument is the result of the createPopup Method
   * @param {*} popup
   */
  removePopup(popup) {
    throw new Error("Abstract function removePopup must be implemented");
  }

  /**
   * Toggles visibility of specified control
   *
   * @param {string} control
   * @param {boolean} enabled
   */
  toggleControl(control, enabled) { }
}
