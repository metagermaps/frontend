import { map } from "./maps/MetaGerMapModule";
import { config } from "./Config";
import { gpsManager } from "./GpsManager";
import { searchModule } from "./SearchModule";
import { LngLatBounds } from "maplibre-gl";
import { fakeGpsModule } from "./FakeGPSModule";
import { routeModule } from "./RouteFinder";
import { navigationModule } from "./NavigationModule";
import { UrlGenerator } from "./UrlGenerator";
import { showAppDeprecation } from "./appDeprecation";

export let module = null; // CUrrent module

$(document).ready(function () {
  $(".inactive").hide();

  // Make sure map is loaded
  map.load().then(() => {
    initializeInterface();
  });
});

function initializeInterface() {
  if (config.init.module != null) {
    switchModule(config.init.module);
  } else if (config.init.vehicle && config.init.waypoints) {
    // Check if the current url points to a route calculation
    switchModule("route-finding", {
      vehicle: config.init.vehicle,
      waypoints: config.init.waypoints.split(";"),
      mapposition: config.init.bbox,
      navigation_active: config.init.navigation_active,
    });
  } else if (config.init.bbox || config.init.query) {
    switchModule("search", {
      query: config.init.query,
      viewbox: config.init.viewbox,
      mapposition: config.init.bbox,
      reverse: config.init.reverse,
      results: [],
      bounded: config.init.bounded,
    });
  } else {
    // Center the map on the users current position since no position for the map is supplied
    switchModule("search");
  }

  if (
    typeof android != "undefined" &&
    typeof android.pageFinished == "function"
  )
    android.pageFinished();

  window.addEventListener("popstate", (event) => {
    event.preventDefault();
    if (event.state && event.state.hasOwnProperty("options") && event.state.options.hasOwnProperty("settings")) {
      if (document.getElementById("settings").open != event.state.options.settings) {
        if (event.state.options.settings) {
          document.getElementById("settings").showModal();
        } else {
          document.getElementById("settings").close();
        }
      }
    }
    // Check which module the state is based on
    switch (event.state.module) {
      case "search":
        let mapposition = null;
        if (event.state.mapposition) {
          mapposition = new LngLatBounds(
            event.state.mapposition._sw,
            event.state.mapposition._ne
          );
        }
        let viewbox = null;
        if (event.state.viewbox) {
          viewbox = new LngLatBounds(
            event.state.viewbox._sw,
            event.state.viewbox._ne
          );
        }
        switchModule("search", {
          query: event.state.query,
          viewbox: viewbox,
          mapposition: mapposition,
          reverse: event.state.reverse,
          popstate: true,
        });
        break;
      case "route-finder":
        switchModule("route-finding", {
          vehicle: event.state.vehicle,
          waypoints: event.state.waypoints,
          mapposition: event.state.mapposition,
          navigation_active: event.state.navigation_active,
          append: false,
        });
        break;
    }
  });

  // Update data timestamps
  (() => {
    let types = ["tiles", "search", "routing"];

    for (let type of types) {
      setInterval(() => {
        updateTimestamp(type);
      }, 300000);
      updateTimestamp(type);
    }

    async function updateTimestamp(type, error_counter = 0) {
      if (error_counter > 9) return false;
      let url = UrlGenerator.to(`/data/${type}`);
      return fetch(url)
        .then((response) => response.json())
        .then((response) => {
          let timestamp = response.timestamp;
          let update_time_string = new Date(timestamp * 1000).toLocaleString();
          document.querySelector(
            `#nav-menu .updates span.data-date-${type}`
          ).textContent = update_time_string;
          return true;
        }).catch(error => {
          console.error(error);
          return new Promise((resolve) => {
            setTimeout(() => {
              resolve(updateTimestamp(type, error_counter + 1));
            }, 10000);
          });
        });
    }

    document.querySelector(
      "#nav-menu .updates span.data-date-timezone"
    ).textContent = Intl.DateTimeFormat().resolvedOptions().timeZone;
  })();

  // Make navbar links functional
  (() => {
    document.querySelectorAll(".control-navbar").forEach(element => {
      element.addEventListener("click", (e) => {
        e.preventDefault();
        let opener = document.getElementById("nav-opener");
        opener.checked = !opener.checked;
      })
    })

    let iframe = document.querySelector("#page iframe");

    document.querySelectorAll("#nav-menu a").forEach((link) => {
      let target = link.href;
      if (target.endsWith("#")) return;
      link.onclick = (e) => {
        if (!target.endsWith("/hilfe") && typeof android == "undefined") return;
        e.preventDefault();
        if (!target.endsWith("/hilfe") && typeof android != "undefined") return;
        iframe.src = target;
        document.querySelector("#page").classList.add("active");
        document.querySelector("#nav-opener").checked = false;
      };
    });
    document.querySelector("#page > div.close-container > button").onclick = (
      e
    ) => {
      iframe.src = "";
      document.querySelector("#page").classList.remove("active");
    };
  })();

  (async () => {
    showAppDeprecation();
  })();

  // GPS error
  (async () => {
    let dialog_container = document.querySelector("#gps-modal");
    let dialog_opener = document.querySelector("#gps-error a");
    if (!dialog_container || !dialog_opener) return;
    dialog_opener.addEventListener("click", e => {
      e.preventDefault();
      dialog_container.showModal();
    });
    dialog_container.querySelector("button.close").addEventListener("click", e => dialog_container.close());
  })();
}

export function switchModule(name, args) {
  if (module !== null) {
    // Every Module must implement this method for deinitialization
    module.exit();
    module = null;
  }

  switch (name) {
    case "search":
      // The search Module can be started with or without a search term
      module = searchModule;
      if (typeof args == "object") {
        module
          .enable(
            args.mapposition,
            args.query,
            args.viewbox,
            args.results,
            args.reverse,
            args.bounded,
            args.search,
            args.stateupdates,
            args.exit_callback
          )
          .then(() => {
            if (!args.hasOwnProperty("popstate")) {
              module.updateState();
            }
          });
      } else {
        module.enable().then(() => {
          module.updateState();
        });
      }
      break;
    case "route-finding":
      module = routeModule;
      if (typeof args == "object") {
        module.enable(
          args.vehicle,
          args.waypoints,
          args.mapposition,
          args.append,
          args.navigation_active
        );
      } else {
        module.enable();
      }
      break;
    case "navigation":
      module = navigationModule;
      module.enable();
      break;
    case "fakegps":
      module = fakeGpsModule;
      module.enable();
      break;
    default:
      return;
  }
}