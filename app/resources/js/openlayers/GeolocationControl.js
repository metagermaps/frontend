import Control from "ol/control/Control";
import { gpsManager } from "../GpsManager";
import { bbox, circle, featureCollection } from "@turf/turf";
import { map } from "../maps/MetaGerMapModule";
import { RenderOptions } from "../maps/RenderOptions";
import { LngLat, LngLatBounds } from "maplibre-gl";
import { GeolocationControlManager } from "../GeolocationControlManager";

export class GeolocationControl extends Control {
  controlManager;

  constructor(opt_options) {
    const options = opt_options || {};

    let container = document.createElement("div");
    container.classList.add(
      "control-geolocation",
      "off",
      "ol-unselectable",
      "ol-control"
    );

    let button = document.createElement("button");
    let span = document.createElement("span");
    span.textContent = "⌖";
    button.appendChild(span);
    container.appendChild(button);

    super({
      element: container,
      target: options.target,
    });
    this.controlManager = new GeolocationControlManager(button);
  }
}
