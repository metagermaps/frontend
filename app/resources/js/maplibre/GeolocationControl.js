import { GeolocationControlManager } from "../GeolocationControlManager";

export class GeolocationControl {
  #controlManager = null;

  onAdd(map) {
    this._map = map;
    let container = document.createElement("div");
    container.classList.add("maplibregl-ctrl", "maplibregl-ctrl-group", "control-geolocation");

    let button = document.createElement("button");
    let span = document.createElement("span");
    span.textContent = "⌖";
    button.appendChild(span);
    container.appendChild(button);

    this.container = container;
    this.#controlManager = new GeolocationControlManager(button);
    return container;
  }
  onRemove(map) {
    this.container.parentNode.removeChild(this.container);
    this.map = undefined;
  }
}
