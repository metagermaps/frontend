import { LngLat } from "maplibre-gl";
import { switchModule } from "./app";
import { map } from "./maps/MetaGerMapModule";
import { along, bearing, bearingToAzimuth, distance, length, lineString, point } from "@turf/turf";
import { config } from "./Config";

class FakeGPSModule {
  htmlelement = document.querySelector("#fakegps-addon");

  current_position = null;
  current_bearing = null;
  current_accuracy = null;
  current_speed = null;

  configured_speed = null;

  // Target Position
  target_position = null;
  target_animation = null;  // Interval for animation

  // Default speeds in km/h
  default_speeds = {
    foot: 7,
    bike: 20,
    car: 50,
  };

  // Event handlers
  leftclick_handler = this.leftclick.bind(this);
  gps_updater = null; // Interval for updating the GPS position in local storage

  user_position_updater = (e) => {
    let center = e.target.getCenter();
    let next_position = turf.point([center.lng, center.lat]);
    let bearing = turf.bearing(this.current_position, next_position);
    if (turf.booleanEqual(this.current_position, this.target_position)) {
      this.target_position = null;
      this.current_speed = 0;
      this.updateCurrentPosition(
        next_position,
        this.current_bearing,
        this.current_accuracy
      );
    } else {
      this.updateCurrentPosition(next_position, bearing, this.current_accuracy);
    }
  };

  constructor() {
    let fakegps_nav = document.querySelector("#nav-fakegps");
    if (!fakegps_nav) return;
    fakegps_nav.onclick = (e) => {
      e.preventDefault();
      switchModule("fakegps");
    };

    this.htmlelement
      .querySelectorAll("input[name=profile]")
      .forEach((element) => {
        element.onchange = (e) => {
          let selected_profile = e.target.value;
          this.htmlelement.querySelector("#speed").value =
            this.default_speeds[selected_profile];
          this.htmlelement.querySelector("#speed").dispatchEvent(new Event("change"));
        };
      });

    // Prevent non numbers in speed input field
    // Update Configured speed
    let speed_input = this.htmlelement.querySelector("#speed");
    this.configured_speed = speed_input.value;
    speed_input.addEventListener("keypress", e => {
      if (e.which < 40 || e.which > 57) e.preventDefault();
    });
    let update_speed = e => {
      let new_speed = parseInt(speed_input.value);
      if (!isNaN(new_speed)) {
        this.configured_speed = new_speed;
      }
    };
    speed_input.addEventListener("keyup", update_speed);
    speed_input.addEventListener("change", update_speed);


    let accuracy_input = this.htmlelement.querySelector("#accuracy");
    this.current_accuracy = parseInt(accuracy_input.value);
    accuracy_input
      .addEventListener("keypress", (e) => {
        if (e.which < 40 || e.which > 57) e.preventDefault();
      });
    let update_accuracy = e => {
      let new_accuracy = parseInt(accuracy_input.value);
      if (isNaN(new_accuracy)) return;
      this.updateCurrentPosition(
        this.current_position,
        this.current_bearing,
        new_accuracy
      );
    }
    accuracy_input.addEventListener("keyup", update_accuracy);
    accuracy_input.addEventListener("change", update_accuracy);

    // Exit button
    this.htmlelement.querySelector(".exit > button").onclick = (e) => {
      switchModule("search");
    };
  }

  updateCurrentPosition(
    point = this.current_position,
    bearing = this.current_bearing,
    accuracy = this.current_accuracy
  ) {
    // Calc distance between points
    this.current_position = point;
    this.current_bearing = bearing;
    this.current_accuracy = accuracy;
    map.showUserPosition(
      new LngLat(
        point.geometry.coordinates[0],
        point.geometry.coordinates[1]
      ),
      bearing,
      accuracy
    );
    map.easeTo({
      center: new LngLat(
        point.geometry.coordinates[0],
        point.geometry.coordinates[1]
      )
    });

    if (this.htmlelement.querySelector("#enable-updates").checked)
      this.storeLocationUpdate(point, bearing, accuracy);
  }

  enable() {
    this.htmlelement.classList.add("active");
    document.getElementById("nav-opener").checked = false;
    this.updateState();

    let selected_profile = this.htmlelement.querySelector(
      "input[name=profile]:checked"
    ).value;
    this.htmlelement.querySelector("#speed").value =
      this.default_speeds[selected_profile];

    if (this.current_position == null) {
      let point = map.getCenter();
      point = turf.point([point.lng, point.lat]);
      this.updateCurrentPosition(
        point,
        this.current_bearing,
        this.current_accuracy
      );
    }

    map.toggleDragpan(false);
    map.toggleScrollzoom(false);
    map.toggleKeyboard(false);
    map.toggleDragRotate(false);
    // Update state when moving
    map.on("moveend", this.updateState);
    // Add Listener to left Click
    map.on("click", this.leftclick_handler);

    // start interval for updating the current position
    this.gps_updater = setInterval(() => {
      if (!this.target_position) {
        this.current_speed = 0;
        this.updateCurrentPosition();
      }
    }, 2500);
  }

  leftclick(event) {
    if (event.coordinate) {
      event.lngLat = new LngLat(event.coordinate[0], event.coordinate[1]);
    }
    let target = turf.point([event.lngLat.lng, event.lngLat.lat]);
    if (!this.current_position || distance(this.current_position, target) > 2) {
      // Distance too long just directly update the target position
      this.target_position = null;  // Stop animation if one is running
      this.updateCurrentPosition(target, null, this.current_accuracy);
      map.jumpTo({
        center: new LngLat(
          target.geometry.coordinates[0],
          target.geometry.coordinates[1]
        ),
      });
    } else {
      this.animatePosition(target);
    }
  }

  animatePosition(target) {
    if (!this.current_position) {
      this.updateCurrentPosition(target, null, this.current_accuracy);
      return;
    }

    this.target = target;
    if (this.target_animation != null) return;
    let last_update = new Date().getTime();
    this.target_animation = setInterval(() => {
      if (this.current_speed != this.configured_speed)
        this.current_speed = this.configured_speed;
      // Exit condition
      // Calculate next point on route - Calculate 
      let animation_line = lineString([this.current_position, this.target]);
      let time_since_last_update = (new Date().getTime() - last_update) / 1000 / 3600;
      let km_since_last_update = this.current_speed * time_since_last_update;
      if (length(animation_line) <= km_since_last_update || this.target == null) {
        clearInterval(this.target_animation);
        this.target_animation = null;
        this.updateCurrentPosition(this.target);
        this.target = null;
        return;
      }

      let new_position = along(animation_line, km_since_last_update);
      let new_bearing = bearing(this.current_position, new_position);
      this.updateCurrentPosition(new_position, new_bearing, this.current_accuracy);
      last_update = new Date().getTime();
    }, 250);
  }

  storeLocationUpdate(point, heading = null, accuracy) {
    let speed = this.current_speed;
    if (speed == 0) {
      heading = "NaN";
    } else {
      // Convert from km/h to m/s
      speed = speed * (5 / 18);
    }
    if (heading && heading != "NaN") {
      // The supplied heading is -180 to 180 degrees.
      // We need to convert it to 0 to 360 degrees
      heading = bearingToAzimuth(heading);
    }

    // Generate a random point according to the accuracy
    let circle = turf.circle(point, accuracy / 1000);
    let bbox = turf.bbox(circle);
    do {
      let random_x = Math.random() * (bbox[2] - bbox[0]) + bbox[0];
      let random_y = Math.random() * (bbox[3] - bbox[1]) + bbox[1];
      point = turf.point([random_x, random_y]);
    } while (!turf.booleanPointInPolygon(point, circle));

    // Fake Geolocation Coordinates as geolocation api would return them
    // According to https://developer.mozilla.org/en-US/docs/Web/API/GeolocationCoordinates
    let geolocation_data = {
      timestamp: Date.now(),
      coords: {
        longitude: point.geometry.coordinates[0],
        latitude: point.geometry.coordinates[1],
        altitude: null,
        accuracy: accuracy,
        heading: heading,
        speed: speed,
      },
    };

    localStorage.setItem("location", JSON.stringify(geolocation_data));
  }

  exit() {
    this.htmlelement.classList.remove("active");
    map.hideUserPosition();
    map.off("click", this.leftclick_handler);
    map.toggleDragpan(true);
    map.toggleScrollzoom(true);
    map.toggleKeyboard(true);
    map.toggleDragRotate(true);

    if (this.gps_updater) clearInterval(this.gps_updater);
  }

  updateState() {
    let current_bounds = map.getBounds();
    let url = `/${config.localization.current_locale}/fakegps/${JSON.stringify(current_bounds.toArray())}`;
    let state = {
      module: "fakegps",
      mapposition: current_bounds,
      timestamp: Date.now(),
    };

    // Skip update if too frequest
    let udpate_rate_ms = 2500;
    if (
      window.history.state &&
      state.timestamp - window.history.state.timestamp < udpate_rate_ms
    ) {
      return;
    }

    if (window.location.pathname.match(/^\/fakegps/)) {
      window.history.replaceState(state, "", url);
    } else {
      window.history.pushState(state, "", url);
    }
  }
}

export const fakeGpsModule = new FakeGPSModule();
