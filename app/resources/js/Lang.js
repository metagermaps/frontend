import i18next from "i18next";
import { config } from "./Config";

import en from "../../lang/en.json";
import de from "../../lang/de.json";
import da from "../../lang/da.json";
import es from "../../lang/es.json";
import fi from "../../lang/fi.json";
import fr from "../../lang/fr.json";
import it from "../../lang/it.json";
import nl from "../../lang/nl.json";
import pl from "../../lang/pl.json";
import sv from "../../lang/sv.json";

i18next.init({
  lng: config.localization.current_locale,
  fallbackLng: config.localization.fallback_locale,
  supportedLngs: config.localization.supported_locales,
  returnEmptyString: false,
  defaultNS: 'translation',
  fallbackNS: 'translation',
  debug: config.env != "production",
  // allow keys to be phrases having `:`, `.`
  nsSeparator: false,
  keySeparator: false,
  resources: {
    en: {
      translation: en,
    },
    de: {
      translation: de,
    },
    da: {
      translation: da,
    },
    es: {
      translation: es,
    },
    fi: {
      translation: fi,
    },
    fr: {
      translation: fr,
    },
    it: {
      translation: it,
    },
    nl: {
      translation: nl,
    },
    pl: {
      translation: pl,
    },
    sv: {
      translation: sv,
    },
  },
});

export const lang = i18next;
