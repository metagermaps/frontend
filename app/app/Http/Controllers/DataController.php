<?php

namespace App\Http\Controllers;

use Cache;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class DataController extends Controller
{
    public function tiles()
    {
        $url = config("maps.tileserver.host") . "/data.json";

        $cache_key = "data:tiles:" . md5($url);
        if (Cache::has($cache_key)) {
            return response()->json(Cache::get($cache_key));
        }

        $ch = curl_init($url);
        curl_setopt_array($ch, [
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT_MS => 500
        ]);
        $result = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($code === 200) {
            $result = json_decode($result);
            $data_time = new Carbon($result[0]->{"planetiler:osm:osmosisreplicationtime"});
            $result = ["timestamp" => $data_time->unix()];
            Cache::put($cache_key, $result, now()->addMinute());
            return response()->json(["timestamp" => $data_time->unix()], 200, ["Cache-Control" => "max-age=3600"]);
        } else {
            abort(404);
        }
    }

    public function search()
    {
        $url = config("maps.nominatim.host") . "/status?format=json";

        $cache_key = "data:search:" . md5($url);
        if (Cache::has($cache_key)) {
            return response()->json(Cache::get($cache_key));
        }

        $ch = curl_init($url);
        curl_setopt_array($ch, [
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT_MS => 500
        ]);
        $result = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($code === 200) {
            $result = json_decode($result);
            $data_time = new Carbon($result->data_updated);
            $result = ["timestamp" => $data_time->unix()];
            Cache::put($cache_key, $result, now()->addMinute());
            return response()->json(["timestamp" => $data_time->unix()], 200, ["Cache-Control" => "max-age=3600"]);
        } else {
            abort(404);
        }
    }

    public function routing()
    {
        $url = config("maps.routingserver.host") . "/info";

        $cache_key = "data:search:" . md5($url);
        if (Cache::has($cache_key)) {
            return response()->json(Cache::get($cache_key));
        }

        $ch = curl_init($url);
        curl_setopt_array($ch, [
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT_MS => 500
        ]);
        $result = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($code === 200) {
            $result = json_decode($result);
            $data_time = new Carbon($result->data_date);
            $result = ["timestamp" => $data_time->unix()];
            //Cache::put($cache_key, $result, now()->addMinute());
            return response()->json(["timestamp" => $data_time->unix()], 200, ["Cache-Control" => "max-age=3600"]);
        } else {
            abort(404);
        }
    }
}
