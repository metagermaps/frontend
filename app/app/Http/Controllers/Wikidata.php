<?php

namespace App\Http\Controllers;

use App;
use App\Models\CachedFetcher;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use URL;

class Wikidata extends Controller
{
    const API_URL = "https://www.wikidata.org/w/api.php";

    public function get(Request $request, string $locale, string $id)
    {
        $response = self::FETCH_WIKIDATA($id);
        if ($response != null) {
            return response()->json($response, 200, ["Content-Type" => "application/json", "Cache-Control" => "max-age=7200"]);
        } else {
            abort(404);
        }
    }

    public static function FETCH_WIKIDATA(string $id): ?array
    {
        $languages = [App::currentLocale()];
        if (App::currentLocale() !== App::getFallbackLocale()) {
            $languages[] = App::getFallbackLocale();
        }
        $params = [
            "action" => "wbgetentities",
            "languages" => implode(",", $languages),    // Only DE for nwo
            "format" => "json",
            "ids" => $id
        ];
        $url = self::API_URL . "?" . http_build_query($params);
        $response = CachedFetcher::FETCH($url);

        if ($response["http_code"] !== 200) {
            return null;
        }

        $data = json_decode($response["body"]);

        if (!property_exists($data, "entities") || !property_exists($data->entities, $id)) {
            return [];
        }
        $data = $data->entities->$id;

        $population = null;
        if (property_exists($data->claims, "P1082")) {
            $population_data = $data->claims->P1082;
            if (sizeof($population_data) > 0) {
                $population = floatval(self::getLatestClaim($population_data)->mainsnak->datavalue->value->amount);
            }
        }

        $area = null;
        if (property_exists($data->claims, "P2046")) {
            $area_data = $data->claims->P2046;
            if (sizeof($area_data) > 0) {
                $area = floatval(self::getLatestClaim($area_data)->mainsnak->datavalue->value->amount);
            }
        }

        $website = null;
        if (property_exists($data->claims, "P856")) {
            $website_data = $data->claims->P856;
            if (sizeof($website_data) > 0) {
                $website = self::getLocalizedClaim($website_data, $languages)->mainsnak->datavalue->value;
            }
        }

        $images = [];
        if (property_exists($data->claims, "P41")) {
            $images["flag"] = self::parseImage(self::getLatestClaim($data->claims->P41), $id);
        }
        if (property_exists($data->claims, "P2716")) {
            $images["collage"] = self::parseImage(self::getLatestClaim($data->claims->P2716), $id);
        }
        if (property_exists($data->claims, "P8592")) {
            $images["aerial"] = self::parseImage(self::getLatestClaim($data->claims->P8592), $id);
        }

        $label = "";
        if (property_exists($data->labels, $languages[0])) {
            $label = $data->labels->{$languages[0]}->value;
        } else if (sizeof($languages) >= 2 && property_exists($data->labels, $languages[1])) {
            $label = $data->labels->{$languages[1]}->value;
        }

        $description = "";
        if (property_exists($data->descriptions, $languages[0])) {
            $description = $data->descriptions->{$languages[0]}->value;
        } else if (sizeof($languages) >= 2 && property_exists($data->descriptions, $languages[1])) {
            $description = $data->descriptions->{$languages[1]}->value;
        }

        return [
            "label" => $label,
            "description" => $description,
            "population" => $population,
            "area" => $area,
            "website" => $website,
            "images" => $images,
        ];
    }

    private static function getLocalizedClaim(array $wikidata_claims, array $app_locales)
    {
        $locales = [
            "de" => "Q188",
            "en" => "Q1860",
            "es" => "Q1321",
            "da" => "Q9035",
            "fi" => "Q1412",
            "fr" => "Q150",
            "it" => "Q652",
            "nl" => "Q7411",
            "pl" => "Q809",
            "sv" => "Q9027"
        ];
        $used_locales = [];
        foreach ($app_locales as $app_locale) {
            if (array_key_exists($app_locale, $locales)) {
                $used_locales[] = $locales[$app_locale];
            }
        }
        if (sizeof($used_locales) === 0) {
            $used_locales[] = $locales["en"];
        }

        // Always select the most current image if there are multiple
        $wikidata_claim = null;
        try {
            foreach ($wikidata_claims as $claim) {
                if (!property_exists($claim->qualifiers, "P407"))
                    continue;

                foreach ($used_locales as $used_locale) {
                    foreach ($claim->qualifiers->P407 as $language_claim) {
                        if ($language_claim->datavalue->value->id === $used_locale) {
                            $wikidata_claim = $claim;
                            break 2;
                        }
                    }
                }
            }
        } catch (Exception $e) {
        }
        if ($wikidata_claim === null) {
            // If no language ranking exists just take the first entry for now
            $wikidata_claim = $wikidata_claims[0];
        }
        return $wikidata_claim;
    }

    private static function getLatestClaim(array $wikidata_claims)
    {
        // Always select the most current image if there are multiple
        $claim_date = null;
        $wikidata_claim = null;
        try {
            foreach ($wikidata_claims as $claim) {
                if (!property_exists($claim->qualifiers, "P580"))
                    continue;
                $date = $claim->qualifiers->P580[0]->datavalue->value->time;
                $date = Carbon::createFromFormat("\+Y-m-d\TH:i:s\Z", $date);
                if ($claim_date === null || $date->isAfter($claim_date)) {
                    $wikidata_claim = $claim;
                    $claim_date = $date;
                }
            }
        } catch (Exception $e) {
        }
        if ($wikidata_claim === null) {
            // If no date ranking exists just take the last entry for now
            $wikidata_claim = $wikidata_claims[sizeof($wikidata_claims) - 1];
        }
        return $wikidata_claim;
    }

    private static function parseImage(object $wikidata_image, $id): ?array
    {

        $flag_image_name = str_replace(" ", "_", $wikidata_image->mainsnak->datavalue->value);
        $flag_image_url = "https://commons.wikimedia.org/w/index.php?title=Special:Redirect/file/" . urlencode($flag_image_name) . "&width=300";

        $attribution = self::generateAttribution($id, $flag_image_name);

        return [
            "image" => URL::temporarySignedRoute("imageproxy", now()->addWeek(), ["url" => $flag_image_url]),
            "attribution" => $attribution,
        ];

    }

    private static function generateAttribution(string $id, string $wikidata_image_name): ?array
    {
        $image_information_params = [
            "action" => "query",
            "prop" => "imageinfo",
            "iiprop" => "extmetadata",
            "format" => "json",
            "titles" => "File:$wikidata_image_name"
        ];
        $url = self::API_URL . "?" . http_build_query($image_information_params);
        $response = CachedFetcher::FETCH($url);

        if ($response["http_code"] !== 200) {
            return null;
        }
        $image_information = json_decode($response["body"]);
        try {

            $metadata = $image_information->query->pages->{"-1"}->imageinfo[0]->extmetadata;
            $result = [];
            $result["wikidata"] = [
                "name" => $metadata->ObjectName->value,
                "link" => "https://www.wikidata.org/wiki/$id#/media/File:$wikidata_image_name",
            ];
            if ($metadata->LicenseShortName->value === "Public domain") {
                return $result;
            }

            $result["artist"] = $metadata->Artist->value;
            $result["license"] = [
                "name" => $metadata->LicenseShortName->value,
                "link" => $metadata->LicenseUrl->value
            ];
            return $result;

        } catch (Exception $e) {
            return null;
        }
    }
}
