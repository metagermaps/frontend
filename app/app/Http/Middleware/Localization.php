<?php

namespace App\Http\Middleware;

use App;
use Closure;
use Exception;
use GeoIp2\Database\Reader;
use Illuminate\Http\Request;
use Locale;
use ResourceBundle;
use Symfony\Component\HttpFoundation\Response;
use URL;

class Localization
{
    const SUPPORTED_LOCALES = ["de", "en", "da", "es", "fi", "fr", "it", "nl", "pl", "sv"];

    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $locale = "en";

        if ($request->filled("locale") && in_array($request->input("locale"), self::SUPPORTED_LOCALES)) {
            $locale = $request->input("locale");
        } else if ($request->cookie("locale", null) !== null && in_array($request->cookie("locale"), self::SUPPORTED_LOCALES)) {
            $locale = $request->cookie("locale");
        } else {
            $locale = self::getDefaultLocale();
        }

        App::setLocale($locale);
        App::setFallbackLocale("en");
        URL::defaults(["locale" => $locale]);

        return $next($request);
    }

    public static function getUnlocalizedUri(): string
    {
        $url = \Illuminate\Support\Facades\Request::getRequestUri();
        foreach (self::SUPPORTED_LOCALES as $supported_locale) {
            if (stripos($url, "/$supported_locale") === 0) {
                return str_ireplace("/$supported_locale", "", $url);
            }
        }
        return $url;
    }

    /**
     * Guesses a locale based on the Accept-Language Header
     * and/or IP Addres Location
     */
    public static function getDefaultLocale(): string
    {
        $default_locale = "en";
        $preferred_language = \Illuminate\Support\Facades\Request::getPreferredLanguage(self::SUPPORTED_LOCALES);
        if ($preferred_language != null && $preferred_language !== "en") {
            $default_locale = $preferred_language;
        } else {
            try {
                $cityDBReader = new Reader(storage_path("app/public/GeoLite2-City.mmd"));
                $record = $cityDBReader->city(\Illuminate\Support\Facades\Request::ip());
                if (!$record->traits->isAnonymous) {
                    $country = $record->registeredCountry->isoCode;
                    $lang = self::getLanguage($country);
                    if (in_array($lang, self::SUPPORTED_LOCALES)) {
                        $default_locale = $lang;
                    }
                }
            } catch (Exception $e) {
            }
        }
        return $default_locale;
    }

    private static function getLanguage(string $country): string
    {
        $subtags = ResourceBundle::create('likelySubtags', 'ICUDATA', false);
        $country = Locale::canonicalize('und_' . $country);
        if (($country[0] ?? null) === '_') {
            $country = 'und' . $country;
        }
        $locale = $subtags->get($country) ?: $subtags->get('und');
        return Locale::getPrimaryLanguage($locale . "tes");
    }
}
