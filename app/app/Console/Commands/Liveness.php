<?php

namespace App\Console\Commands;

use App\Http\Controllers\LivenessController;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Redis;

class Liveness extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'liveness';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends a liveness signal for healthchecks';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info("done");
        Redis::setex(LivenessController::LIVENESS_SCHEDULER_KEY, 90, true);
        return 0;
    }
}
