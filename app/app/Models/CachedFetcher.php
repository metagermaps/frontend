<?php
namespace App\Models;

use Cache;

class CachedFetcher
{
    const USERAGENT = "MetaGer Maps (support@metager.de)";

    static function FETCH($url, $cache_key_prefix = "", $curl_opts = [])
    {
        $cache_key = $cache_key_prefix . md5($url);

        $cached = Cache::get($cache_key);
        if ($cached === null) {
            $ch = curl_init($url);
            $curlopts_default = [
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HEADER => true,
                CURLOPT_USERAGENT => self::USERAGENT,
                CURLOPT_TIMEOUT_MS => 5000
            ];
            curl_setopt_array($ch, $curlopts_default + $curl_opts);
            $response = curl_exec($ch);
            $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
            $header_text = substr($response, 0, $header_size);
            $headers = [];
            foreach (explode(PHP_EOL, $header_text) as $index => $header) {
                if ($index === 0)
                    continue;
                $header_array = explode(":", $header);
                if (sizeof($header_array) != 2)
                    continue;
                if (stripos($header_array[0], "set-cookie") !== false) {
                    continue;
                }
                $headers[trim($header_array[0])] = trim($header_array[1]);
            }
            $body = substr($response, $header_size);
            $response_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            $response = [
                "http_code" => $response_code,
                "headers" => $headers,
                "body" => $body
            ];
            if ($response_code === 200) {
                Cache::put($cache_key, $response, now()->addHours(2));
            }
            return $response;
        } else {
            return $cached;
        }
    }
}