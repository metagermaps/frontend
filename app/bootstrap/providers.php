<?php

return [
    App\Providers\AppServiceProvider::class,
    LaravelLang\JsonFallback\TranslationServiceProvider::class
];